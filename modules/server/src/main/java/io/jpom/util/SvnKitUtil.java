package io.jpom.util;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import io.jpom.build.BuildUtil;
import io.jpom.model.data.RepositoryModel;
import io.jpom.model.enums.GitProtocolEnum;
import io.jpom.system.JpomRuntimeException;
import org.tmatesoft.svn.core.*;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.internal.wc.SVNFileUtil;
import org.tmatesoft.svn.core.wc.*;

import java.io.File;

/**
 * svn 工具
 * <p>
 * https://www.cnblogs.com/lekko/p/6005382.html
 *
 * @author bwcx_jzy
 * @date 2019/8/6
 */
public class SvnKitUtil {

	static {
		// 初始化库。 必须先执行此操作。具体操作封装在setupLibrary方法中。
		/*
		 * For using over http:// and https://
		 */
		DAVRepositoryFactory.setup();
		/*
		 * For using over svn:// and svn+xxx://
		 */
		SVNRepositoryFactoryImpl.setup();

		/*
		 * For using over file:///
		 */
		FSRepositoryFactory.setup();
	}

	private static final DefaultSVNOptions OPTIONS = SVNWCUtil.createDefaultOptions(true);

	/**
	 * 对SVNKit连接进行认证，并获取连接
	 *
	 * @param repositoryModel 仓库
	 */
	public static SVNClientManager getAuthClient(RepositoryModel repositoryModel) {
		Integer protocol = repositoryModel.getProtocol();
		String userName = repositoryModel.getUserName();
		String password = StrUtil.emptyToDefault(repositoryModel.getPassword(), StrUtil.EMPTY);
		//
		ISVNAuthenticationManager authManager;
		//
		if (protocol == GitProtocolEnum.HTTP.getCode()) {
			authManager = SVNWCUtil.createDefaultAuthenticationManager(userName, password.toCharArray());
		} else if (protocol == GitProtocolEnum.SSH.getCode()) {
			File dir = SVNWCUtil.getDefaultConfigurationDirectory();
			// ssh
			File rsaFile = BuildUtil.getRepositoryRsaFile(repositoryModel);
			char[] pwdEmpty = StrUtil.EMPTY.toCharArray();
			if (rsaFile == null) {
				authManager = SVNWCUtil.createDefaultAuthenticationManager(dir, userName, pwdEmpty, null, pwdEmpty, true);
			} else {
				if (StrUtil.isEmpty(password)) {
					authManager = SVNWCUtil.createDefaultAuthenticationManager(dir, userName, pwdEmpty, rsaFile, pwdEmpty, true);
				} else {
					authManager = SVNWCUtil.createDefaultAuthenticationManager(dir, userName, pwdEmpty, rsaFile, password.toCharArray(), true);
				}
			}
		} else {
			throw new IllegalStateException("不支持到协议类型");
		}
		// 实例化客户端管理类
		return SVNClientManager.newInstance(OPTIONS, authManager);
	}

	/**
	 * 判断当前仓库url是否匹配
	 *
	 * @param wcDir           仓库路径
	 * @param repositoryModel 仓库
	 * @return true 匹配
	 * @throws SVNException 异常
	 */
	private static Boolean checkUrl(File wcDir, RepositoryModel repositoryModel) throws SVNException {
		String url = repositoryModel.getGitUrl();
		// 实例化客户端管理类
		SVNClientManager clientManager = getAuthClient(repositoryModel);
		try {
			// 通过客户端管理类获得updateClient类的实例。
			SVNWCClient wcClient = clientManager.getWCClient();
			SVNInfo svnInfo = null;
			do {
				try {
					svnInfo = wcClient.doInfo(wcDir, SVNRevision.HEAD);
				} catch (SVNException svn) {
					if (svn.getErrorMessage().getErrorCode() == SVNErrorCode.FS_NOT_FOUND) {
						checkOut(clientManager, url, wcDir);
					} else {
						throw svn;
					}
				}
			} while (svnInfo == null);
			String reUrl = svnInfo.getURL().toString();
			return reUrl.equals(url);
		} finally {
			clientManager.dispose();
		}
	}

	/**
	 * SVN检出
	 *
	 * @param repositoryModel 仓库
	 * @param targetPath      目录
	 * @return Boolean
	 * @throws SVNException svn
	 */
	public static String checkOut(RepositoryModel repositoryModel, File targetPath) throws SVNException {
		// 实例化客户端管理类
		SVNClientManager ourClientManager = getAuthClient(repositoryModel);
		try {
			if (targetPath.exists()) {
				if (!FileUtil.file(targetPath, SVNFileUtil.getAdminDirectoryName()).exists()) {
					if (!FileUtil.del(targetPath)) {
						FileUtil.del(targetPath.toPath());
					}
				} else {
					// 判断url是否变更
					if (!checkUrl(targetPath, repositoryModel)) {
						if (!FileUtil.del(targetPath)) {
							FileUtil.del(targetPath.toPath());
						}
					} else {
						ourClientManager.getWCClient().doCleanup(targetPath);
					}
				}
			}
			return checkOut(ourClientManager, repositoryModel.getGitUrl(), targetPath);
		} finally {
			ourClientManager.dispose();
		}
	}

	private static String checkOut(SVNClientManager ourClientManager, String url, File targetPath) throws SVNException {
		// 通过客户端管理类获得updateClient类的实例。
		SVNUpdateClient updateClient = ourClientManager.getUpdateClient();
		updateClient.setIgnoreExternals(false);
		// 相关变量赋值
		SVNURL svnurl = SVNURL.parseURIEncoded(url);
		try {
			// 要把版本库的内容check out到的目录
			long workingVersion = updateClient.doCheckout(svnurl, targetPath, SVNRevision.HEAD, SVNRevision.HEAD, SVNDepth.INFINITY, true);
			return String.format("把版本：%s check out ", workingVersion);
		} catch (SVNAuthenticationException s) {
			throw new JpomRuntimeException("账号密码不正确", s);
		}
	}
}
