package io.jpom.common;

/**
 * @author Hotstrip
 * Const class
 */
public class Const {
	/**
	 * String const
	 */
	public static final String ID_STR = "id";
	public static final String GROUP_STR = "group";
	public static final String GROUP_COLUMN_STR = "`group`";


	/**
	 * 应用程序类型的配置 key
	 */
	public static final String APPLICATION_NAME = "spring.application.name";

	/**
	 * String get
	 */
	public static final String GET_STR = "get";


	/**
	 * id_rsa
	 */
	public static final String ID_RSA = "_id_rsa";
	/**
	 * sshkey
	 */
	public static final String SSH_KEY = "sshkey";
}
